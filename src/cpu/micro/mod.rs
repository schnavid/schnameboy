use crate::{cpu::alu::AluAction, cpu::registers::Register, cpu::MemOp};
use std::fmt::{Debug, Formatter};

pub mod instructions;

/// - 4 bits Data dest from Bus
/// - 4 bits Data source for Bus
/// - 4 bits ALU OP
///   - None, ADD, ADC, SUB, SBC, AND, OR, XOR, CD, INC, DEC
/// - 2 bits MEM OP
///   - 0: IDLE
///   - 1: READ
///   - 2: WRITE
///   - 3: FETCH (Increments PC)
/// - 1 bit LAST INSTR
///   - If this is the last Microinstruction for this Macroinstruction (set new instr from fetch)
#[derive(Copy, Clone)]
pub struct MicroInstruction(u16);

impl Debug for MicroInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:016b}", self.0)
    }
}

impl MicroInstruction {
    fn new(
        dest: Register,
        source: Register,
        alu: AluAction,
        mem: MemOp,
        last: bool,
    ) -> MicroInstruction {
        let dest = dest as u16;
        let dest = dest << 12;

        let source = source as u16;
        let source = source << 8;

        let alu = alu as u16;
        let alu = alu << 4;

        let mem = mem as u16;
        let mem = mem << 2;

        let last = last as u16;
        let last = last << 1;

        MicroInstruction(source + dest + alu + mem + last)
    }

    pub fn dest(self) -> Register {
        match (self.0 & 0b1111_0000_0000_0000) >> 12 {
            0 => Register::B,
            1 => Register::C,
            2 => Register::D,
            3 => Register::E,
            4 => Register::H,
            5 => Register::L,
            6 => Register::A,
            7 => Register::F,
            8 => Register::PC,
            9 => Register::SP,
            10 => Register::MemDat,
            _ => panic!("invalid register id"),
        }
    }

    pub fn source(self) -> Register {
        match (self.0 & 0b0000_1111_0000_0000) >> 8 {
            0 => Register::B,
            1 => Register::C,
            2 => Register::D,
            3 => Register::E,
            4 => Register::H,
            5 => Register::L,
            6 => Register::A,
            7 => Register::F,
            8 => Register::PC,
            9 => Register::SP,
            10 => Register::MemDat,
            _ => panic!("invalid register id"),
        }
    }

    pub fn alu(self) -> AluAction {
        match (self.0 & 0b0000_0000_1111_0000) >> 4 {
            1 => AluAction::ADD,
            2 => AluAction::ADC,
            3 => AluAction::SUB,
            4 => AluAction::SBC,
            5 => AluAction::AND,
            6 => AluAction::OR,
            7 => AluAction::XOR,
            8 => AluAction::CP,
            9 => AluAction::INC,
            10 => AluAction::DEC,
            _ => AluAction::None,
        }
    }

    pub fn mem(self) -> MemOp {
        match (self.0 & 0b0000_0000_0000_1100) >> 2 {
            1 => MemOp::Read,
            2 => MemOp::Write,
            3 => MemOp::Fetch,
            _ => MemOp::Idle,
        }
    }

    pub fn last(self) -> bool {
        (self.0 & 0b0000_0000_0000_0010) > 0
    }
}
