use crate::cpu::{alu::AluAction::*, micro::MicroInstruction, registers::Register::*, MemOp::*};
use std::collections::BTreeMap;

pub fn micro_instructions() -> BTreeMap<u16, MicroInstruction> {
    let nop = MicroInstruction::new(B, B, None, Fetch, true);

    let stop = MicroInstruction::new(B, B, None, Fetch, true);

    // 8-Bit Transfer and I/O Instructions

    let ld_b_b = MicroInstruction::new(B, B, None, Fetch, true);

    let ld_b_c = MicroInstruction::new(B, C, None, Fetch, true);

    let ld_b_d = MicroInstruction::new(B, D, None, Fetch, true);

    let ld_b_e = MicroInstruction::new(B, E, None, Fetch, true);

    let ld_b_h = MicroInstruction::new(B, H, None, Fetch, true);

    let ld_b_l = MicroInstruction::new(B, L, None, Fetch, true);

    let ld_b_a = MicroInstruction::new(B, A, None, Fetch, true);

    let ld_c_b = MicroInstruction::new(C, B, None, Fetch, true);

    let ld_c_c = MicroInstruction::new(C, C, None, Fetch, true);

    let ld_c_d = MicroInstruction::new(C, D, None, Fetch, true);

    let ld_c_e = MicroInstruction::new(C, E, None, Fetch, true);

    let ld_c_h = MicroInstruction::new(C, H, None, Fetch, true);

    let ld_c_l = MicroInstruction::new(C, L, None, Fetch, true);

    let ld_c_a = MicroInstruction::new(D, A, None, Fetch, true);

    let ld_d_b = MicroInstruction::new(D, B, None, Fetch, true);

    let ld_d_c = MicroInstruction::new(D, C, None, Fetch, true);

    let ld_d_d = MicroInstruction::new(D, D, None, Fetch, true);

    let ld_d_e = MicroInstruction::new(D, E, None, Fetch, true);

    let ld_d_h = MicroInstruction::new(D, H, None, Fetch, true);

    let ld_d_l = MicroInstruction::new(D, L, None, Fetch, true);

    let ld_d_a = MicroInstruction::new(D, A, None, Fetch, true);

    let ld_e_b = MicroInstruction::new(E, B, None, Fetch, true);

    let ld_e_c = MicroInstruction::new(E, C, None, Fetch, true);

    let ld_e_d = MicroInstruction::new(E, D, None, Fetch, true);

    let ld_e_e = MicroInstruction::new(E, E, None, Fetch, true);

    let ld_e_h = MicroInstruction::new(E, H, None, Fetch, true);

    let ld_e_l = MicroInstruction::new(E, L, None, Fetch, true);

    let ld_e_a = MicroInstruction::new(E, A, None, Fetch, true);

    let ld_h_b = MicroInstruction::new(H, B, None, Fetch, true);

    let ld_h_c = MicroInstruction::new(H, C, None, Fetch, true);

    let ld_h_d = MicroInstruction::new(H, D, None, Fetch, true);

    let ld_h_e = MicroInstruction::new(H, E, None, Fetch, true);

    let ld_h_h = MicroInstruction::new(H, H, None, Fetch, true);

    let ld_h_l = MicroInstruction::new(H, L, None, Fetch, true);

    let ld_h_a = MicroInstruction::new(H, A, None, Fetch, true);

    let ld_l_b = MicroInstruction::new(L, B, None, Fetch, true);

    let ld_l_c = MicroInstruction::new(L, C, None, Fetch, true);

    let ld_l_d = MicroInstruction::new(L, D, None, Fetch, true);

    let ld_l_e = MicroInstruction::new(L, E, None, Fetch, true);

    let ld_l_h = MicroInstruction::new(L, H, None, Fetch, true);

    let ld_l_l = MicroInstruction::new(L, L, None, Fetch, true);

    let ld_l_a = MicroInstruction::new(L, A, None, Fetch, true);

    let ld_a_b = MicroInstruction::new(A, B, None, Fetch, true);

    let ld_a_c = MicroInstruction::new(A, C, None, Fetch, true);

    let ld_a_d = MicroInstruction::new(A, D, None, Fetch, true);

    let ld_a_e = MicroInstruction::new(A, E, None, Fetch, true);

    let ld_a_h = MicroInstruction::new(A, H, None, Fetch, true);

    let ld_a_l = MicroInstruction::new(A, L, None, Fetch, true);

    let ld_a_a = MicroInstruction::new(A, A, None, Fetch, true);

    let ld_b_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_b_n2 = MicroInstruction::new(B, MemDat, None, Fetch, true);

    let ld_c_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_c_n2 = MicroInstruction::new(C, MemDat, None, Fetch, true);

    let ld_d_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_d_n2 = MicroInstruction::new(D, MemDat, None, Fetch, true);

    let ld_e_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_e_n2 = MicroInstruction::new(E, MemDat, None, Fetch, true);

    let ld_h_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_h_n2 = MicroInstruction::new(H, MemDat, None, Fetch, true);

    let ld_l_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_l_n2 = MicroInstruction::new(L, MemDat, None, Fetch, true);

    let ld_a_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let ld_a_n2 = MicroInstruction::new(A, MemDat, None, Fetch, true);

    // 8-Bit Arithmetic and Logical Operation Instructions

    let add_a_b = MicroInstruction::new(B, B, ADD, Fetch, true);

    let add_a_c = MicroInstruction::new(B, C, ADD, Fetch, true);

    let add_a_d = MicroInstruction::new(B, D, ADD, Fetch, true);

    let add_a_e = MicroInstruction::new(B, E, ADD, Fetch, true);

    let add_a_h = MicroInstruction::new(B, H, ADD, Fetch, true);

    let add_a_l = MicroInstruction::new(B, L, ADD, Fetch, true);

    let add_a_a = MicroInstruction::new(B, A, ADD, Fetch, true);

    let add_a_n1 = MicroInstruction::new(B, B, None, Fetch, false);
    let add_a_n2 = MicroInstruction::new(B, MemDat, ADD, Fetch, true);

    let inc_b = MicroInstruction::new(B, B, INC, Fetch, true);

    let inc_c = MicroInstruction::new(C, C, INC, Fetch, true);

    let inc_d = MicroInstruction::new(D, D, INC, Fetch, true);

    let inc_e = MicroInstruction::new(E, E, INC, Fetch, true);

    let inc_h = MicroInstruction::new(H, H, INC, Fetch, true);

    let inc_l = MicroInstruction::new(L, L, INC, Fetch, true);

    let inc_a = MicroInstruction::new(A, A, INC, Fetch, true);

    let dec_b = MicroInstruction::new(B, B, DEC, Fetch, true);

    let dec_c = MicroInstruction::new(C, C, DEC, Fetch, true);

    let dec_d = MicroInstruction::new(D, D, DEC, Fetch, true);

    let dec_e = MicroInstruction::new(E, E, DEC, Fetch, true);

    let dec_h = MicroInstruction::new(H, H, DEC, Fetch, true);

    let dec_l = MicroInstruction::new(L, L, DEC, Fetch, true);

    let dec_a = MicroInstruction::new(A, A, DEC, Fetch, true);

    #[rustfmt::skip]
    let instructions = [
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![inc_b],   vec![dec_b],   vec![ld_b_n1, ld_b_n2],   vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![inc_c],  vec![dec_c],  vec![ld_c_n1, ld_c_n2],  vec![nop],
    vec![stop],    vec![nop],     vec![nop],     vec![nop],     vec![inc_d],   vec![dec_d],   vec![ld_d_n1, ld_d_n2],   vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![inc_e],  vec![dec_e],  vec![ld_e_n1, ld_e_n2],  vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![inc_h],   vec![dec_h],   vec![ld_h_n1, ld_h_n2],   vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![inc_l],  vec![dec_l],  vec![ld_l_n1, ld_l_n2],  vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![inc_a],  vec![dec_a],  vec![ld_a_n1, ld_a_n2],  vec![nop],
    vec![ld_b_b],  vec![ld_b_c],  vec![ld_b_d],  vec![ld_b_e],  vec![ld_b_h],  vec![ld_b_l],  vec![nop],                vec![ld_b_a],  vec![ld_c_b], vec![ld_c_c], vec![ld_c_d], vec![ld_c_e], vec![ld_c_h], vec![ld_c_l], vec![nop],               vec![ld_c_a],
    vec![ld_d_b],  vec![ld_d_c],  vec![ld_d_d],  vec![ld_d_e],  vec![ld_d_h],  vec![ld_d_l],  vec![nop],                vec![ld_d_a],  vec![ld_e_b], vec![ld_e_c], vec![ld_e_d], vec![ld_e_e], vec![ld_e_h], vec![ld_e_l], vec![nop],               vec![ld_e_a],
    vec![ld_h_b],  vec![ld_h_c],  vec![ld_h_d],  vec![ld_h_e],  vec![ld_h_h],  vec![ld_h_l],  vec![nop],                vec![ld_h_a],  vec![ld_l_b], vec![ld_l_c], vec![ld_l_d], vec![ld_l_e], vec![ld_l_h], vec![ld_l_l], vec![nop],               vec![ld_l_a],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![ld_a_b], vec![ld_a_c], vec![ld_a_d], vec![ld_a_e], vec![ld_a_h], vec![ld_a_l], vec![nop],               vec![ld_a_a],
    vec![add_a_b], vec![add_a_c], vec![add_a_d], vec![add_a_e], vec![add_a_h], vec![add_a_l], vec![nop],                vec![add_a_a], vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![add_a_n1, add_a_n2], vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],     vec![nop],                vec![nop],     vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],    vec![nop],               vec![nop],
    ];

    let mut m = BTreeMap::new();

    for (op_code, micros) in instructions.iter().enumerate() {
        for (cycle, micro) in micros.iter().enumerate() {
            m.insert(((op_code << 8) + cycle) as u16, *micro);
        }
    }

    m
}
