use crate::cpu::{registers::Register, Cpu};

#[derive(Debug, PartialOrd, PartialEq)]
pub enum AluAction {
    None,
    ADD,
    ADC,
    SUB,
    SBC,
    AND,
    OR,
    XOR,
    CP,
    INC,
    DEC,
}

pub fn alu(cpu: &mut Cpu, action: AluAction, dest: Register) {
    fn set_dest(cpu: &mut Cpu, dest: Register, result: u8) {
        match dest {
            Register::B => cpu.registers.b = result,
            Register::C => cpu.registers.c = result,
            Register::D => cpu.registers.d = result,
            Register::E => cpu.registers.e = result,
            Register::H => cpu.registers.h = result,
            Register::L => cpu.registers.l = result,
            Register::A => cpu.registers.a = result,
            Register::F => cpu.registers.f = result,
            Register::PC => cpu.registers.pc = result as u16,
            Register::SP => cpu.registers.sp = result as u16,
            Register::MemDat => cpu.registers.mem_dat = result,
        }
    }

    match action {
        AluAction::ADD => {
            let result = cpu.registers.a + cpu.temp;

            cpu.registers.set_z_flag(result == 0);
            cpu.registers
                .set_cy_flag((result as u16) < ((cpu.registers.a as u16) + (cpu.temp as u16)));
            cpu.registers.set_n_flag(false);
            cpu.registers
                .set_h_flag((result & 0x0F) < ((cpu.registers.a & 0x0F) + (cpu.temp & 0x0F)));

            cpu.registers.a = result;
        }
        AluAction::ADC => {
            let result = cpu.registers.a + cpu.temp + cpu.registers.cy_flag() as u8;

            cpu.registers.set_z_flag(result == 0);
            cpu.registers
                .set_cy_flag((result as u16) < ((cpu.registers.a as u16) + (cpu.temp as u16)));
            cpu.registers.set_n_flag(false);
            cpu.registers.set_h_flag(
                (result & 0x0F)
                    < ((cpu.registers.a & 0x0F)
                        + (cpu.temp & 0x0F)
                        + cpu.registers.cy_flag() as u8),
            );

            cpu.registers.a = result;
        }
        AluAction::INC => {
            let result = cpu.temp + 1;

            cpu.registers.set_z_flag(result == 0);
            cpu.registers
                .set_cy_flag((result as u16) < ((cpu.temp as u16) + 1));
            cpu.registers.set_n_flag(false);
            cpu.registers
                .set_h_flag((result & 0x0F) < ((cpu.temp & 0x0F) + 1));

            set_dest(cpu, dest, result);
        }
        AluAction::DEC => {
            let result = cpu.temp - 1;

            cpu.registers.set_z_flag(result == 0);
            cpu.registers.set_n_flag(true);
            cpu.registers
                .set_h_flag((result & 0x0F) < ((cpu.temp & 0x0F) - 1));

            set_dest(cpu, dest, result);
        }
        _ => {}
    };
}
