#[derive(Debug, Default)]
pub struct Registers {
    pub(crate) a:       u8,
    pub(crate) b:       u8,
    pub(crate) c:       u8,
    pub(crate) d:       u8,
    pub(crate) e:       u8,
    pub(crate) f:       u8,
    pub(crate) h:       u8,
    pub(crate) l:       u8,
    pub(crate) pc:      u16,
    pub(crate) sp:      u16,
    pub(crate) mem_dat: u8,
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub enum Register {
    B,
    C,
    D,
    E,
    H,
    L,
    A,
    F,
    PC,
    SP,
    MemDat,
}

impl Registers {
    pub fn bc(&self) -> u16 {
        ((self.b as u16) << 8) | (self.c as u16)
    }

    pub fn de(&self) -> u16 {
        ((self.d as u16) << 8) | (self.e as u16)
    }

    pub fn hl(&self) -> u16 {
        ((self.h as u16) << 8) | (self.l as u16)
    }

    pub fn z_flag(&self) -> bool {
        self.f & 0b1000_0000 > 0
    }

    pub fn n_flag(&self) -> bool {
        self.f & 0b0100_0000 > 0
    }

    pub fn h_flag(&self) -> bool {
        self.f & 0b0010_0000 > 0
    }

    pub fn cy_flag(&self) -> bool {
        self.f & 0b0001_0000 > 0
    }

    pub fn set_z_flag(&mut self, flag: bool) {
        self.f |= (flag as u8) << 7
    }

    pub fn set_n_flag(&mut self, flag: bool) {
        self.f |= (flag as u8) << 6
    }

    pub fn set_h_flag(&mut self, flag: bool) {
        self.f |= (flag as u8) << 5
    }

    pub fn set_cy_flag(&mut self, flag: bool) {
        self.f |= (flag as u8) << 4
    }

    pub fn get_register(&self, r: u8) -> u8 {
        match r {
            0b111 => self.a,
            0b000 => self.b,
            0b001 => self.c,
            0b010 => self.d,
            0b011 => self.e,
            0b100 => self.h,
            0b101 => self.l,
            _ => 0,
        }
    }
}
