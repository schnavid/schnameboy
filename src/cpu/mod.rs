use crate::cpu::alu::AluAction;
use crate::cpu::control_store::ControlStore;
use crate::cpu::registers::{Register, Registers};

pub mod alu;
pub mod control_store;
pub mod micro;
pub mod registers;

pub enum MemOp {
    Idle,
    Read,
    Write,
    Fetch,
}

pub struct Cpu {
    registers: Registers,
    temp:      u8,

    // addr:      u16,
    instruction: u8,
    cycle:       u8,

    current_mem_op: MemOp,
    ram:            Box<[u8; 0xFFFF]>,
}

impl Default for Cpu {
    fn default() -> Self {
        Cpu::new()
    }
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            registers: Registers::default(),
            temp:      0x0,

            // addr:      0x0,
            instruction: 0x0,
            cycle:       0x0,

            current_mem_op: MemOp::Idle,
            ram:            Box::new([0; 0xFFFF]),
        }
    }

    pub fn read_ram(&self, addr: u16) -> u8 {
        self.ram[addr as usize]
    }

    pub fn write_ram(&mut self, addr: u16, data: u8) {
        self.ram[addr as usize] = data;
    }

    pub fn load_program(&mut self, program: Vec<u8>) {
        for (i, x) in program.iter().enumerate() {
            self.write_ram(0x150 + i as u16, *x);
        }
    }

    pub fn clock_cycle(&mut self) -> bool {
        let instr = ControlStore::get_micro(((self.instruction as u16) << 8) + self.cycle as u16);

        println!("{:?}", instr);

        let mut tcycle = 1;
        loop {
            match tcycle {
                1 => {
                    match instr.mem() {
                        MemOp::Idle => {}
                        MemOp::Read => {
                            self.current_mem_op = instr.mem();
                        }
                        MemOp::Write => {}
                        MemOp::Fetch => {
                            self.registers.pc += 1;
                            self.current_mem_op = instr.mem();
                        }
                    }

                    tcycle += 1;
                }
                2 => {
                    match instr.source() {
                        Register::B => self.temp = self.registers.b,
                        Register::C => self.temp = self.registers.c,
                        Register::D => self.temp = self.registers.d,
                        Register::E => self.temp = self.registers.e,
                        Register::H => self.temp = self.registers.h,
                        Register::L => self.temp = self.registers.l,
                        Register::A => self.temp = self.registers.a,
                        Register::F => self.temp = self.registers.f,
                        Register::PC => {}
                        Register::SP => {}
                        Register::MemDat => self.temp = self.registers.mem_dat,
                    }

                    println!("SOURCE: {:?}, TMP: {:02x}", instr.source(), self.temp);

                    tcycle += 1;
                }
                3 => {
                    alu::alu(self, instr.alu(), instr.dest());

                    if instr.alu() == AluAction::None {
                        match instr.dest() {
                            Register::B => self.registers.b = self.temp,
                            Register::C => self.registers.c = self.temp,
                            Register::D => self.registers.d = self.temp,
                            Register::E => self.registers.e = self.temp,
                            Register::H => self.registers.h = self.temp,
                            Register::L => self.registers.l = self.temp,
                            Register::A => self.registers.a = self.temp,
                            Register::F => self.registers.f = self.temp,
                            Register::PC => self.registers.pc = self.temp as u16,
                            Register::SP => self.registers.sp = self.temp as u16,
                            Register::MemDat => self.registers.mem_dat = self.temp,
                        }

                        println!("DEST: {:?}", instr.dest());
                    } else {
                        println!("ALU: {:?}", instr.alu());
                    }

                    tcycle += 1;
                }
                4 => {
                    match instr.mem() {
                        MemOp::Idle => {}
                        MemOp::Read => {}
                        MemOp::Write => {}
                        MemOp::Fetch => {
                            let val = self.read_ram(self.registers.pc);
                            if instr.last() {
                                self.instruction = val;
                                self.cycle = 0;
                            } else {
                                self.registers.mem_dat = val;
                                self.cycle += 1;
                            }
                        }
                    }

                    tcycle += 1;
                }
                _ => {
                    break;
                }
            }
        }

        println!("{:?}", self.registers);

        self.instruction != 0x10
    }

    pub fn run(&mut self) {
        let mut running = true;

        self.registers.pc = 0x150;

        self.instruction = self.read_ram(self.registers.pc);

        while running {
            for _ in 0..70_224 {
                running = self.clock_cycle();

                if !running {
                    break;
                }
            }
        }
    }
}
