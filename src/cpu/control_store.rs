use crate::cpu::micro::{instructions::micro_instructions, MicroInstruction};
use lazy_static::lazy_static;
use std::collections::BTreeMap;

pub struct ControlStore {
    micro_store: BTreeMap<u16, MicroInstruction>,
}

impl ControlStore {
    fn new() -> ControlStore {
        ControlStore {
            micro_store: micro_instructions(),
        }
    }

    pub fn get_micro(instr: u16) -> MicroInstruction {
        println!("microinstr: {:04x}", instr);
        CONTROL_STORE.micro_store[&instr]
    }
}

lazy_static! {
    static ref CONTROL_STORE: ControlStore = ControlStore::new();
}
