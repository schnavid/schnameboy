use crate::cpu::Cpu;

pub mod cpu;

fn main() {
    let mut cpu = Cpu::new();

    #[rustfmt::skip]
    cpu.load_program(vec![
        0x06, 0x03, // LD   B, 3
        0x3E, 0x05, // LD   A, 5
        0x80,       // ADD  A, B
        0xC6, 0x07, // ADD  A, 7
        0x5F,       // LD   E, A
        0x10,       // STOP
        0x00,       // NOP
    ]);

    cpu.run();
}
